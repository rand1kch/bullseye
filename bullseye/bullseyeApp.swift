//
//  bullseyeApp.swift
//  bullseye
//
//  Created by Даниил Тимонин on 27.09.2022.
//

import SwiftUI

@main
struct bullseyeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
